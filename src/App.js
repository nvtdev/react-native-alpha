import React, { Component } from 'react';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import firebase from 'firebase';
import Router from './Router';

class App extends Component {
  componentWillMount() {
    // Initialize Firebase
    var config = {
      apiKey: "AIzaSyAmvYmhwd762kHrNXY00vVofZg0-Wx_udY",
      authDomain: "alpha-65e31.firebaseapp.com",
      databaseURL: "https://alpha-65e31.firebaseio.com",
      projectId: "alpha-65e31",
      storageBucket: "alpha-65e31.appspot.com",
      messagingSenderId: "452367228429"
    };
    firebase.initializeApp(config);
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
