import {
  TEST_ACTION
} from './types';

export const testAction = () => {
  return (dispatch) => {
    console.log('TEST ACTION');
    dispatch({ type: TEST_ACTION });
  };
};
