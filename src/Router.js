import React from 'react';
import { Actions, Scene, Router } from 'react-native-router-flux';
import HomePage from './components/HomePage';

const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 55 }}>
    <Scene key="mainScene">
      <Scene
        key="homePage"
        component={HomePage}
        title="Home"
        initial
      />
    </Scene>
    </Router>
  );
};

export default RouterComponent;
